export const environment = {
  production: true,
  API_URL: 'http://localhost:8082',
  DASHBOARD_ROTATION_TIMEOUT: 3000,
};
