import { OfferLatestVM } from './offer-latest.model';

export class OfferWithBids extends OfferLatestVM {
    maxPrice: number;

    bidsCount: number;

    bidsValue: number;
}
