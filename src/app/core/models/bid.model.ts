export class BidModel {
    public offerId: string;

    public price: number;

    public qty: number;

    constructor(copyFrom: {
        price?: number,
        qty?: number,
    } = {}) {
        this.price = copyFrom.price || 0;
        this.qty = copyFrom.qty || 0;
    }
}
