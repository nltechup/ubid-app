export class CategoryVM {
    id: string;
    name: string;
    link: string;
}
