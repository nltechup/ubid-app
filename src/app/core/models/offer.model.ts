import * as moment from 'moment';

export class OfferModel {
    // tslint:disable-next-line:variable-name
    public id: string;

    // tslint:disable-next-line:variable-name
    public cat_uuid: string;

    public product: string;

    public description: string;

    public quantity: number;

    public photoUrl: string;

    public price: number;

    public end: Date;

    constructor(copyFrom: {
        id?: string,
        cat_uuid?: string,
        product?: string,
        description?: string,
        quantity?: number,
        photoUrl?: string,
        price?: number,
        end?: Date,
        time?: string, } = {}) {
            const now = moment();

            this.id = copyFrom.id;
            this.cat_uuid = copyFrom.cat_uuid;
            this.product = copyFrom.product || '';
            this.description = copyFrom.description || '';
            this.quantity = copyFrom.quantity;
            this.photoUrl = copyFrom.photoUrl || '';
            this.price = copyFrom.price;

            const expirationTime = moment(copyFrom.time || now.clone().format('HH:mm'), 'HH:mm');
            const expirationDate = moment(copyFrom.end || now.clone().add(1, 'day')).startOf('day');

            this.end = expirationDate.set({ hour: expirationTime.get('hour'), minute: expirationTime.get('minute'), second: 0 }).toDate();
    }
}
