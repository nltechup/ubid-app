export class OfferLatestVM {
    public id: string;

    public link: string;

    public title: string;

    public description: string;

    public quantity: number;

    public category: string;

    public photoUrl: string;

    public createdAt: Date;

    public end: Date;

    public price: number;

    public expired: boolean;
}
