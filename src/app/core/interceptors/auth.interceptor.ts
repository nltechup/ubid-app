import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthService } from '../services';
import { Token } from '../models';

@Injectable({
    providedIn: 'root',
})
export class AuthInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService, private router: Router) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let result: Observable<HttpEvent<any>> = null;

        this.authService.getJwtToken()
            .subscribe((token: Token) => {
                let headers = req.headers.set('x-token', token.toString());

                if (!req.headers.get('Content-Type')) {
                    headers = headers.set('Content-Type', 'application/json');
                }

                const authRequest = req.clone({
                    headers,
                });

                result = next.handle(authRequest)
                    .pipe(
                        catchError((response: HttpErrorResponse) => {
                            if (response.status === 401) {
                                this.router.navigate(['/auth/login']);
                            }

                            return throwError(response);
                        }),
                    );
            }, (err) => {
                // console.log(`Auth error: ${err}`);
            });

        return result;
    }
}

