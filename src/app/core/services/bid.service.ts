import { Observable } from 'rxjs';

import { BidModel } from '../models';

export abstract class BidService {
    abstract getMyBids(): Observable<BidModel[]>;
    abstract getBidsForOffer(offerId: string): Observable<{ count: number }>;
    abstract saveBid(model: BidModel): Observable<any>;
}
