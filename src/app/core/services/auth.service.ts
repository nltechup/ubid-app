import { Observable } from 'rxjs';

import { Token, AuthResult } from '../models';

export abstract class AuthService {
    abstract isLoggedIn(): Observable<boolean>;

    abstract onAuthStatusChanged(): Observable<boolean>;

    abstract getJwtToken(): Observable<Token>;

    abstract logout(): Observable<AuthResult>;

    abstract login(model: any): Observable<AuthResult>;
}
