import { Observable } from 'rxjs';
import { OfferLatestVM } from '../models';

export abstract class SearchService {
    abstract onSearchSubmit(): Observable<OfferLatestVM[]>;
}
