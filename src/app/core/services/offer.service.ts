import { Observable } from 'rxjs';

import { OfferLatestVM, OfferModel, OfferWithBids } from '../models';

export abstract class OfferService {
    abstract getOffer(offerId: string): Observable<OfferLatestVM>;

    abstract getLatestOffers(): Observable<{ title: string, values: OfferLatestVM[] }>;

    abstract getExpiringOffers(): Observable<{ title: string, values: OfferLatestVM[] }>;

    // tslint:disable-next-line:variable-name
    abstract getByCatLink(cat_link: string): Observable<{ title: string, values: OfferLatestVM[] }>;

    abstract getMyOffers(): Observable<OfferWithBids[]>;

    abstract saveOffer(model: OfferModel): Observable<any>;
}
