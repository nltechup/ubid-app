import { Observable } from 'rxjs';

import { CategoryVM } from '../models/category-view.model';

export abstract class CategoryService {
    abstract getCategoriesForMenu(): Observable<CategoryVM[]>;

    abstract getCategories(): Observable<CategoryVM[]>;
}
