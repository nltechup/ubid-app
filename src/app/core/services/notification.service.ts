export abstract class NotificationService {
    abstract displaySuccessMsg(message: string): void;

    abstract displayErrorMsg(message: string): void;
}
