import { Observable } from 'rxjs';

export abstract class MenuService {
    abstract onMenuItemSelected(): Observable<any>;
}
