import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { tap } from 'rxjs/operators';

import { AuthService } from '../services/auth.service';

@Injectable({
    providedIn: 'root',
})
export class AuthGuard implements CanActivate {
    constructor(private authService: AuthService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      return this.authService.isLoggedIn()
        .pipe(tap(authenticated => {
          if (! authenticated) {
            this.router.navigate(['auth/login'], { queryParams: { redirectUrl: state.url } });
          }
        }),
      );
    }
}
