import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { OfferLatestVM } from '../core/models';
import { OfferService, SearchService } from '../core/services';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public latest$: Observable<OfferLatestVM[]>;
  public expiring$: Observable<OfferLatestVM[]>;
  constructor(private offerService: OfferService, private searchService: SearchService) { }

  ngOnInit() {
    this.latest$ = this.offerService.getLatestOffers()
                    .pipe(
                      map(val => val.values),
                    );

    this.expiring$ = this.offerService.getExpiringOffers()
                      .pipe(
                        map(val => val.values),
                      );
    this.searchService.onSearchSubmit()
                        .subscribe((data: any) => {
                          const value = data.term;
                        });
  }
}
