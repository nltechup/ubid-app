import { NbMenuService } from '@nebular/theme';
import { Observable } from 'rxjs';

import { MenuService } from '../core/services';

export class InfrastructureMenuService implements MenuService {
    constructor(private readonly menuService: NbMenuService) { }

    onMenuItemSelected(): Observable<any> {
        return this.menuService.onItemClick();
    }
}
