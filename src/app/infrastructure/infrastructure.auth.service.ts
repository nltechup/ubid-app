import { NbAuthService, NbAuthResult } from '@nebular/auth';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthService } from '../core/services';
import { Token, AuthResult } from '../core/models';

export class InfrastructureAuthService implements AuthService {
    constructor(private readonly authService: NbAuthService) { }

    public isLoggedIn(): Observable<boolean> {
        return this.authService.isAuthenticated();
    }

    public onAuthStatusChanged(): Observable<boolean> {
        return this.authService.onAuthenticationChange();
    }

    public getJwtToken(): Observable<Token> {
        return this.authService.getToken();
    }

    public logout(): Observable<AuthResult> {
        return this.authService.logout('email')
            .pipe(
                map((result: NbAuthResult) => ({ success: result.isSuccess() })),
            );
    }

    public login(model: any): Observable<AuthResult> {
        return this.authService.authenticate('email', model)
            .pipe(
                map((result: NbAuthResult) => ({ success: result.isSuccess() })),
            );
    }
}
