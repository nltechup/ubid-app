import { NotificationService } from '../core/services';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';

export class InfrastructureNotificationService implements NotificationService {
    constructor(private readonly notifService: NbToastrService) { }
    displaySuccessMsg(message: string): void {
        this.notifService.success(message, 'Succes', { position: NbGlobalPhysicalPosition.TOP_RIGHT, status: 'success' });
    }

    displayErrorMsg(message: string): void {
        this.notifService.danger(message, 'Eroare', { position: NbGlobalPhysicalPosition.TOP_RIGHT, status: 'danger' });
    }
}
