import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, race, timer } from 'rxjs';
import { map } from 'rxjs/operators';

import { BidModel } from '../core/models';
import { BidService } from '../core/services/bid.service';
import { environment } from '../../environments/environment';
import { DEFAULT_HTTP_TIMEOUT } from '../util';

@Injectable({
    providedIn: 'root',
})
export class InfrastructureBidService implements BidService {
    constructor(private httpClient: HttpClient) {}

    public getMyBids(): Observable<BidModel[]> {
        return this.httpClient.get<BidModel[]>(`${environment.API_URL}/api/bids/mine`, { observe: 'body' });
    }

    public getBidsForOffer(offerId: string): Observable<{ count: number }> {
        const defaultTimeoutValue = -1;

        return  race(this.httpClient.get<any>(`${environment.API_URL}/api/bids/count?offerId=${offerId}`, { observe: 'body' }),
                timer(DEFAULT_HTTP_TIMEOUT)
                    .pipe(
                        map(_ => ({ count: defaultTimeoutValue })),
                    ),
                );
    }

    public saveBid(model: BidModel): Observable<any> {
        return this.httpClient.post(`${environment.API_URL}/api/bids`, model);
    }
}
