import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { CategoryVM } from '../core/models/category-view.model';
import { CategoryService } from '../core/services/category.service';

@Injectable({
    providedIn: 'root',
})
export class InfrastructureCategoryService implements CategoryService {
    constructor(private httpClient: HttpClient) {}

    public getCategoriesForMenu(): Observable<CategoryVM[]> {
        return this.httpClient.get<CategoryVM[]>(`${environment.API_URL}/api/categories?view=vm`, { observe: 'body' });
    }

    public getCategories(): Observable<CategoryVM[]> {
        return this.httpClient.get<CategoryVM[]>(`${environment.API_URL}/api/categories?view=offer`, { observe: 'body' });
    }
}
