import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SearchService } from '../core/services/search.service';
import { OfferLatestVM } from '../core/models';
import { environment } from '../../environments/environment';
import { NbSearchService } from '@nebular/theme';
import { switchMap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class InfrastructureSearchService implements SearchService {
    constructor(private httpClient: HttpClient, private searchService: NbSearchService) {}
    private search(searchText: string): Observable<OfferLatestVM[]> {
        return this.httpClient.get<OfferLatestVM[]>(`${environment.API_URL}/api/offers?key=${ searchText }`, { observe: 'body' });
    }

    public onSearchSubmit(): Observable<OfferLatestVM[]> {
        return this.searchService.onSearchSubmit()
            .pipe(
                switchMap((value: { term: string, tag?: string }) => this.search(value.term)),
            );
    }
}
