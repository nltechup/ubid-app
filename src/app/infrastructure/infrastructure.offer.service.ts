import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { OfferService } from '../core/services';
import { OfferLatestVM, OfferModel } from '../core/models';
import { environment } from '../../environments/environment';
import { OfferWithBids } from '../core/models/offer-with-bids.model';

@Injectable({
    providedIn: 'root',
})
export class InfrastructureOfferService implements OfferService {
    constructor(private httpClient: HttpClient) {}

    public getOffer(offerId: string): Observable<OfferLatestVM> {
        return this.httpClient.get<OfferLatestVM>(`${environment.API_URL}/api/offers/${offerId}`, { observe: 'body' });
    }

    public getLatestOffers(): Observable<{ title: string, values: OfferLatestVM[] }> {
        const timeStamp = new Date().getTime();
        // tslint:disable-next-line:max-line-length
        return this.httpClient.get<{ title: string, values: OfferLatestVM[] }>(`${environment.API_URL}/api/offers?view=latest`, { observe: 'body' });
    }

    public getExpiringOffers(): Observable<{ title: string, values: OfferLatestVM[] }> {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.get<{ title: string, values: OfferLatestVM[] }>(`${environment.API_URL}/api/offers?view=expiring`, { observe: 'body' });
    }

    // tslint:disable-next-line:variable-name
    public getByCatLink(cat_link: string): Observable<{ title: string, values: OfferLatestVM[] }> {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.get<{ title: string, values: OfferLatestVM[] }>(`${environment.API_URL}/api/offers?cat_link=${cat_link}`, { observe: 'body' });
    }

    public getMyOffers(): Observable<OfferWithBids[]> {
        return this.httpClient.get<OfferWithBids[]>(`${environment.API_URL}/api/offers/me`, { observe: 'body' });
    }

    public saveOffer(model: OfferModel): Observable<any> {
        return this.httpClient.post(`${environment.API_URL}/api/offers`, model);
    }
}
