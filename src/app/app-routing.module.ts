import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './auth/login/login.component';
import { LogoutComponent } from './auth/logout/logout.component';
import { AuthGuard } from './core/guards/auth.guard';

const routes: Routes = [
  { path: 'home', component: DashboardComponent },
  { path: 'auth/login', component: LoginComponent, },
  { path: 'auth/logout', component: LogoutComponent, canActivate: [ AuthGuard, ] },
  { path: 'offers', loadChildren: () => import('../app/offers/offers.module').then(m => m.OffersModule) },
  { path: 'bids', loadChildren: () => import('../app/bids/bids.module').then(m => m.BidsModule) },
  { path: 'profile', loadChildren: () => import('../app/profile/profile.module').then(m => m.ProfileModule) },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload', useHash: true }, ) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
