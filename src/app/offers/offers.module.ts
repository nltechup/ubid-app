import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OffersRoutingModule } from './offers-routing.module';
import { OfferListComponent } from './offer-list/offer-list.component';
import { OfferListPresentationComponent } from './offer-list/offer-list-presentation.component';
import { SharedModule } from '../shared/shared.module';
import { OfferCreateComponent } from './offer-create/offer-create.component';
import { OfferCreatePresentationComponent } from './offer-create/offer-create-presentation.component';
import { ReactiveFormsModule } from '@angular/forms';
import { OfferDetailComponent } from './offer-detail/offer-detail.component';
import { OfferDetailPresentationComponent } from './offer-detail/offer-detail-presentation.component';

@NgModule({
  declarations: [
    OfferListComponent,
    OfferListPresentationComponent,
    OfferCreateComponent,
    OfferCreatePresentationComponent,
    OfferDetailComponent,
    OfferDetailPresentationComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    OffersRoutingModule,
    ReactiveFormsModule,
  ],
})
export class OffersModule { }
