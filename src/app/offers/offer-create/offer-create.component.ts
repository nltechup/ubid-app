import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as moment from 'moment';

import { CategoryService, OfferService } from '../../core/services';
import { CategoryVM, OfferModel } from '../../core/models';
import { NotificationService } from '../../core/services/notification.service';

@Component({
  selector: 'app-offer-create',
  templateUrl: './offer-create.component.html',
  styleUrls: ['./offer-create.component.scss']
})
export class OfferCreateComponent implements OnInit, OnDestroy {
  private destroy$: Subject<void> = new Subject();
  public categories$: Observable<CategoryVM[]>;
  public minDate: Date;
  public maxDate: Date;
  public offerForm: FormGroup;
  public model: OfferModel;

  constructor(private categoryService: CategoryService,
              private offerService: OfferService,
              private fb: FormBuilder,
              private notificationService: NotificationService,
              private router: Router) {
    this.model = new OfferModel();
    this.initForm();
  }

  ngOnInit() {
    this.categories$ = this.categoryService.getCategories();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private initForm(): void {
    this.offerForm = this.fb.group({
      cat_uuid: [ this.model.cat_uuid, [ Validators.required, Validators.minLength(1), ] ],
      product: [ this.model.product || '', [ Validators.required, Validators.minLength(5), Validators.maxLength(50), ] ],
      description: [ this.model.description || '', [ Validators.required, Validators.minLength(5), Validators.maxLength(100), ] ],
      quantity: [ this.model.quantity || '', [ Validators.required, Validators.min(1), Validators.max(9999), ] ],
      photoUrl: [ this.model.photoUrl || '', [ Validators.required, ] ],
      price: [ this.model.price, [ Validators.required, Validators.min(0), Validators.max(99999), ] ],
      end: [ this.model.end, [ Validators.required, ] ],
      time: [ moment(this.model.end).format('HH:mm'), [ Validators.required, ] ],
    });
  }

  public onOfferSaved(form: FormGroup): void {
    const model: OfferModel = new OfferModel(form.value);

    this.offerService.saveOffer(model)
      .pipe(
        takeUntil(this.destroy$),
      )
      .subscribe(val => {
        this.model = new OfferModel();
        this.notificationService.displaySuccessMsg('Ați salvat cu succes oferta');
        this.router.navigate(['/']);
      }, (err) => {
        this.notificationService.displayErrorMsg(err.error.message);
      });
  }

  public onCancelPressed(): void {
    this.router.navigate(['/']);
  }
}
