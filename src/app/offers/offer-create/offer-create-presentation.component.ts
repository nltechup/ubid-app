import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { CategoryVM } from '../../core/models';

@Component({
  selector: 'app-offer-create-presentation',
  templateUrl: './offer-create-presentation.component.html',
  styleUrls: ['./offer-create-presentation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OfferCreatePresentationComponent implements OnInit {
  @Input() categories: CategoryVM[] = [];
  @Input() offerForm: FormGroup;
  @Output() formSaved: EventEmitter<FormGroup> = new EventEmitter();
  @Output() cancelPressed: EventEmitter<void> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public onSubmit() {
    if (this.offerForm.valid) {
      this.formSaved.emit(this.offerForm);
    }
  }

  public onCancel() {
    this.cancelPressed.emit();
  }
}
