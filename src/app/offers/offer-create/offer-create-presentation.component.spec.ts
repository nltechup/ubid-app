import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferCreatePresentationComponent } from './offer-create-presentation.component';

describe('OfferCreatePresentationComponent', () => {
  let component: OfferCreatePresentationComponent;
  let fixture: ComponentFixture<OfferCreatePresentationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfferCreatePresentationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferCreatePresentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
