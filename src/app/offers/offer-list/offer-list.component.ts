import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';

import { OfferService } from '../../core/services';
import { OfferLatestVM } from '../../core/models';

@Component({
  selector: 'app-offer-list',
  templateUrl: './offer-list.component.html',
  styleUrls: ['./offer-list.component.scss']
})
export class OfferListComponent implements OnInit {
  public offersByCat$: Observable<{ title: string, values: OfferLatestVM[] }>;

  constructor(private route: ActivatedRoute, private offerService: OfferService) { }

  ngOnInit() {
    this.route.params.subscribe((value: Params) => {
      this.offersByCat$ = this.offerService.getByCatLink(value.cat_link);
    });
  }
}
