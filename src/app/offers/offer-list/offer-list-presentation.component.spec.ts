import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferListPresentationComponent } from './offer-list-presentation.component';

describe('OfferListPresentationComponent', () => {
  let component: OfferListPresentationComponent;
  let fixture: ComponentFixture<OfferListPresentationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfferListPresentationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferListPresentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
