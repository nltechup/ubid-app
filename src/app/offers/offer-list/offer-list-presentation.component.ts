import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

import { OfferLatestVM } from '../../core/models';

@Component({
  selector: 'app-offer-list-presentation',
  templateUrl: './offer-list-presentation.component.html',
  styleUrls: ['./offer-list-presentation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OfferListPresentationComponent implements OnInit {
  @Input() offers: { title: string, values: OfferLatestVM[] };
  @Output() loadPreviousPage: EventEmitter<any> = new EventEmitter();
  @Output() loadNextPage: EventEmitter<any> = new EventEmitter();
  public pageSize = 10;
  public startPage = 1;
  public pageToLoadNext: number;
  public loadingNext = false;
  public loadingPrevious = false;
  public initialScrollRestoration: ScrollRestoration;

  constructor() { }

  ngOnInit() {
    this.pageToLoadNext = this.startPage;
  }

  public loadNext() {
    this.loadNextPage.emit();
  }

  public loadPrevious() {
    this.loadPreviousPage.emit();
  }
}
