import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { OfferService } from '../../core/services';
import { OfferLatestVM } from '../../core/models';

@Component({
  selector: 'app-offer-detail',
  templateUrl: './offer-detail.component.html',
  styleUrls: ['./offer-detail.component.scss']
})
export class OfferDetailComponent implements OnInit, OnDestroy {
  public offer$: Observable<OfferLatestVM>;

  private destroy$: Subject<void> = new Subject();

  constructor(private route: ActivatedRoute, private offerService: OfferService, private router: Router) { }

  ngOnInit() {
    this.route.params
      .pipe(
        takeUntil(this.destroy$),
      )
      .subscribe(data => {
        const id = data.offerId;

        if (id !== '') {
          this.offer$ = this.offerService.getOffer(id);
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public onBidClicked(offerId: string): void {
    this.router.navigate(['bids', 'create', offerId]);
  }
}
