import { Component, OnInit, ChangeDetectionStrategy, Input, EventEmitter, Output } from '@angular/core';

import { OfferModel } from '../../core/models';

@Component({
  selector: 'app-offer-detail-presentation',
  templateUrl: './offer-detail-presentation.component.html',
  styleUrls: ['./offer-detail-presentation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OfferDetailPresentationComponent implements OnInit {
  @Input() model: OfferModel;

  @Output() bidClicked: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public onBidClicked(offerId: string) {
    this.bidClicked.emit(offerId);
  }
}
