import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferDetailPresentationComponent } from './offer-detail-presentation.component';

describe('OfferDetailPresentationComponent', () => {
  let component: OfferDetailPresentationComponent;
  let fixture: ComponentFixture<OfferDetailPresentationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfferDetailPresentationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferDetailPresentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
