import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { OfferListComponent } from './offer-list/offer-list.component';
import { AuthGuard } from '../core/guards/auth.guard';
import { OfferCreateComponent } from './offer-create/offer-create.component';
import { OfferDetailComponent } from './offer-detail/offer-detail.component';

const routes: Routes = [
    { path: 'create', component: OfferCreateComponent, canActivate: [ AuthGuard ] },
    { path: ':cat_link', component: OfferListComponent },
    { path: 'details/:offerId', component: OfferDetailComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OffersRoutingModule { }
