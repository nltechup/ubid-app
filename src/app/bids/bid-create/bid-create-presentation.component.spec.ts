import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidCreatePresentationComponent } from './bid-create-presentation.component';

describe('BidCreatePresentationComponent', () => {
  let component: BidCreatePresentationComponent;
  let fixture: ComponentFixture<BidCreatePresentationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidCreatePresentationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidCreatePresentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
