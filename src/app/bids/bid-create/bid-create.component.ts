import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject, forkJoin } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { OfferLatestVM, BidModel } from '../../core/models';
import { OfferService, BidService, NotificationService } from '../../core/services';
import { DEFAULT_TIMEOUT_RESPONSE, DEFAULT_CB_OPEN_RESPONSE } from '../../util';

@Component({
  selector: 'app-bid-create',
  templateUrl: './bid-create.component.html',
  styleUrls: ['./bid-create.component.scss']
})
export class BidCreateComponent implements OnInit, OnDestroy {
  private destroy$: Subject<void> = new Subject();
  private model: { price: number, qty: number } = null;
  public offer: OfferLatestVM;
  public bidCount: number | string;
  public bidForm: FormGroup;
  public offerId: string;

  constructor(private route: ActivatedRoute,
              private offerService: OfferService,
              private bidService: BidService,
              private fb: FormBuilder,
              private router: Router,
              private notificationService: NotificationService) {
    this.initModel();
  }

  ngOnInit() {
    this.route.params
    .pipe(
      takeUntil(this.destroy$),
    )
    .subscribe(data => {
      this.offerId = data.offerId;

      if (this.offerId !== '') {
        forkJoin([ this.offerService.getOffer(this.offerId), this.bidService.getBidsForOffer(this.offerId) ])
        .pipe(
          takeUntil(this.destroy$),
        )
        .subscribe((result: [ OfferLatestVM, { count: number } & { break: boolean } ]) => {
          this.offer = result[0];

          if (result[1]) {
            if (Object.getOwnPropertyNames(result[1]).indexOf('count') > -1) {
              if (result[1].count >= 0) {
                this.bidCount = result[1].count;
              } else {
                this.bidCount = DEFAULT_TIMEOUT_RESPONSE;
              }
            } else {
              if (result[1].break) {
                this.bidCount = DEFAULT_CB_OPEN_RESPONSE;
              }
            }
          }

          this.initForm();
        });
      }
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public onBidSaved(form: FormGroup): void {
    const bid = new BidModel(form.value);
    bid.offerId = this.offerId;

    this.bidService.saveBid(bid)
      .subscribe(() => {
          this.initModel();
          this.notificationService.displaySuccessMsg('Ați licitat cu succes');
          this.router.navigate(['/']);
        }, (err) => {
          this.notificationService.displayErrorMsg(`Eroare la licitare: ${err}`);
        });
  }

  public onCancelPressed(event: any): void {
    this.router.navigate(['/']);
  }

  private initForm(): void {
    this.bidForm = this.fb.group({
      price: [ this.model.price, [ Validators.required, Validators.min(this.offer.price), ] ],
      qty: [ this.model.qty, [ Validators.required, Validators.min(1), ] ],
    });
  }

  private initModel(): void {
    this.model = { price: 0, qty: 0 };
  }
}
