import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { OfferLatestVM } from '../../core/models';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-bid-create-presentation',
  templateUrl: './bid-create-presentation.component.html',
  styleUrls: ['./bid-create-presentation.component.scss']
})
export class BidCreatePresentationComponent implements OnInit {
  @Input() offer: OfferLatestVM;
  @Input() bidForm: FormGroup;
  @Input() bidCount: number;
  @Output() formSaved: EventEmitter<FormGroup> = new EventEmitter();
  @Output() cancelPressed: EventEmitter<void> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public onCancel() {
    this.cancelPressed.emit();
  }

  public onSubmit() {
    if (this.bidForm.valid) {
      this.formSaved.emit(this.bidForm);
    }
  }
}
