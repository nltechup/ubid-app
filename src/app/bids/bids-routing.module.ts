import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from '../core/guards/auth.guard';
import { BidCreateComponent } from './bid-create/bid-create.component';

const routes: Routes = [
    { path: 'create/:offerId', component: BidCreateComponent, canActivate: [ AuthGuard ] },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BidsRoutingModule { }
