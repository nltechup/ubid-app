import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BidCreateComponent } from './bid-create/bid-create.component';
import { BidsRoutingModule } from './bids-routing.module';
import { SharedModule } from '../shared/shared.module';
import { BidCreatePresentationComponent } from './bid-create/bid-create-presentation.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [BidCreateComponent, BidCreatePresentationComponent],
  imports: [
    CommonModule,
    SharedModule,
    BidsRoutingModule,
    ReactiveFormsModule,
  ]
})
export class BidsModule { }
