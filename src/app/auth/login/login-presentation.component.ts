import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login-presentation',
  templateUrl: './login-presentation.component.html',
  styleUrls: ['./login-presentation.component.scss']
})
export class LoginPresentationComponent implements OnInit {
  @Input() loginForm: FormGroup;

  @Output() formSaved: EventEmitter<FormGroup> = new EventEmitter();

  @Output() cancelPressed: EventEmitter<void> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public onSubmit() {
    if (this.loginForm.valid) {
      this.formSaved.emit(this.loginForm);
    }
  }

  public onCancel() {
    this.cancelPressed.emit();
  }
}
