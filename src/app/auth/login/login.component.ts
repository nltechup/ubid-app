import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

import { AuthService, NotificationService } from '../../core/services';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  private destroy$: Subject<void> = new Subject();

  private redirectUrl = '';

  public loginForm: FormGroup;

  public model: { email: string, password: string, rememberMe: boolean } = null;

  constructor(private authService: AuthService,
              private fb: FormBuilder,
              private router: Router,
              private notificationService: NotificationService,
              private route: ActivatedRoute) {
    this.initModel();
  }

  ngOnInit() {
    this.initForm();
    this.route.queryParams
      .pipe(
        takeUntil(this.destroy$),
      )
      .subscribe(queryParams => {
        if (queryParams && queryParams.redirectUrl) {
          this.redirectUrl = queryParams.redirectUrl;
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private initModel(): void {
    this.model = { email: '', password: '', rememberMe: false };
  }

  private initForm(): void {
    this.loginForm = this.fb.group({
      email: [ this.model.email || '', [ Validators.required, Validators.email, ] ],
      password: [ this.model.password || '', [ Validators.required, Validators.minLength(5), Validators.maxLength(50), ] ],
      rememberMe: [ this.model.rememberMe || false ],
    });
  }

  public onLoginSaved(form: FormGroup): void {
    this.model = form.value;

    this.authService.login(this.model)
      .subscribe(result => {
        if (result.success) {
          this.initModel();
          // this.notificationService.displaySuccessMsg('Autentificare reușită', 'Succes', { position: 'top-end', status: 'success' });
          this.notificationService.displaySuccessMsg('Autentificare reușită');
          this.router.navigate([this.redirectUrl || '/home']);
        } else {
          // this.notificationService.show('Eroare la autentificare', 'Eroare', { position: 'top-end', status: 'danger' });
          this.notificationService.displayErrorMsg('Eroare la autentificare');
        }
      });
  }

  public onCancelPressed(event: any): void {
    this.router.navigate(['/']);
  }
}
