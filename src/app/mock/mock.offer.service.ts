import { Observable, of } from 'rxjs';

import { OfferService } from '../core/services';
import { CategoryModel, OfferLatestVM, OfferModel, OfferWithBids } from '../core/models';

const CATEGORIES: CategoryModel[] = [{
    uuid: '3645F460-1C46-403B-B4BC-66BD595E2241',
    name: 'Articole nou nascuti',
}, {
    uuid: '3F11D8D5-39D8-45D2-AFAC-6ADE9D3711E3',
    name: 'Auto, Moto',
}, {
    uuid: 'E2ABBC88-C95C-40AC-9476-AC0A618499B2',
    name: 'Jocuri, Console',
}];

const OFFERS: OfferLatestVM[] = [
    {
        id: '1',
        title: 'Scutece Pampers Premium Care',
        description: 'XXL Box, marimea 3, 6 - 10 kg',
        link: '{{app_url}}/offers/scutece-pampers-premium-care',
        photoUrl: 'https://s12emagst.akamaized.net/products/21176/21175380/images/res_4fc022a15bf43543a6f3a982d9395ab2_200x200_9sm3.jpg',
        createdAt: new Date('2019-12-01 10:00:00.000Z'),
        end: new Date('2019-12-10 10:00:00.000Z'),
        price: 100,
        expired: false,
        quantity: 1,
        category: 'Articole nou nascuti',
    }, {
        id: '2',
        title: 'Formula de lapte praf Kinder Organic',
        description: 'Topfer, 500 g, de la 1 an',
        link: '{{app_url}}/offers/formula-de-lapte-praf-kinder-organic',
        photoUrl: 'https://s12emagst.akamaized.net/products/2029/2028585/images/res_892c67ed1f467d80549b7b900b491154_200x200_q0f3.jpg',
        createdAt: new Date('2019-11-13 17:25:00.000Z'),
        end: new Date('2019-12-20 10:00:00.000Z'),
        price: 26.5,
        expired: false,
        quantity: 1,
        category: 'Articole nou nascuti',
    }, {
        id: '3',
        title: 'Set cadou Nivea Fresh Sensation',
        description: 'Gel de dus + sampon micelar + Deodorant',
        link: '{{app_url}}/offers/set-cadou-nivea-fresh-sensation',
        photoUrl: 'https://s12emagst.akamaized.net/products/26369/26368784/images/res_b552ea8eecc96e6ec5c06d364980c133_200x200_u2tg.jpg',
        createdAt: new Date('2019-10-27 16:25:00.000Z'),
        end: new Date('2019-12-31 23:59:59.999Z'),
        price: 56,
        expired: false,
        quantity: 1,
        category: 'Cosmetice',
    }, {
        id: '4',
        title: 'Servetele demachiante',
        description: 'Nivea Visage Cleansing Delicate, 25 buc',
        link: '{{app_url}}/offers/servetele-demachinante',
        photoUrl: 'https://s12emagst.akamaized.net/products/427/426808/images/res_01d354d800c76f0c3896b693e0b6b7a2_200x200_nfn0.jpg',
        createdAt: new Date('2019-11-07 09:20:00.000Z'),
        end: new Date('2019-12-20 12:00:00.000Z'),
        price: 12.99,
        expired: false,
        quantity: 1,
        category: 'Cosmetice',
    }, {
        id: '5',
        title: 'Asics, Pantofi sport unisex cu insertii de plasa',
        description: 'pentru fitness Gel DS, Alb/verde, 5.5',
        link: '{{app_url}}/offers/asics-pantofi-sport-unisex-cu-insertii-de-plasa',
        photoUrl: 'https://s12emagst.akamaized.net/products/19105/19104385/images/res_564e4f5b0161e52307577a1966d83c66_200x200_ir7p.jpg',
        createdAt: new Date('2019-12-03 12:20:00.000Z'),
        end: new Date('2019-12-31 12:00:00.000Z'),
        price: 159,
        expired: false,
        quantity: 1,
        category: 'Articole sportive',
    }, {
        id: '6',
        title: 'Cafea boabe Crema e Aroma',
        description: '1kg Lavazza',
        link: '{{app_url}}/offers/cafea-boabe-crema-e-aroma',
        photoUrl: 'https://s12emagst.akamaized.net/products/2192/2191058/images/res_d8e925839d5186c3f88c68630181dc08_200x200_7ocn.jpg',
        createdAt: new Date('2019-12-10 15:15:00.000Z'),
        end: new Date('2019-12-30 19:00:00.000Z'),
        price: 72,
        expired: false,
        quantity: 1,
        category: 'Alimentare',
    }
];

export class MockOfferService implements OfferService {
    public getOffer(offerId: string): Observable<OfferLatestVM> {
        return of(null);
    }

    public getExpiringOffers(): Observable<{ title: string, values: OfferLatestVM[] }> {
        return of({ title: '', values: OFFERS });
    }

    public getLatestOffers(): Observable<{ title: string, values: OfferLatestVM[] }> {
        return of({ title: '', values: OFFERS });
    }

    // tslint:disable-next-line:variable-name
    public getByCatLink(cat_link: string): Observable<{ title: string, values: OfferLatestVM[] }> {
        return of({ title: '', values: OFFERS });
    }

    public getMyOffers(): Observable<OfferWithBids[]> {
        return of([]);
    }

    public saveOffer(model: OfferModel): Observable<any> {
        return of(true);
    }
}
