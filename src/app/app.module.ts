import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbAuthModule, NbAuthService, NbPasswordAuthStrategy, NbAuthJWTToken } from '@nebular/auth';
import { NbThemeModule, NbLayoutModule, NbMenuModule, NbMenuService, NbToastrService, NbSearchService } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';

import { CoreModule } from './core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from './shared/shared.module';
import { environment } from '../environments/environment';
import { AuthService, OfferService, CategoryService, BidService, MenuService, NotificationService, SearchService } from './core/services';
import { InfrastructureOfferService, InfrastructureCategoryService, InfrastructureBidService, InfrastructureNotificationService, InfrastructureAuthService, InfrastructureMenuService, InfrastructureSearchService } from './infrastructure';
import { AuthModule } from './auth/auth.module';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: 'email',
          baseEndpoint: environment.API_URL,
          token: {
            class: NbAuthJWTToken,
          },
          login: {
            endpoint: '/api/auth/login',
            method: 'POST',
            redirect: {
              success: '/',
              failure: null,
            }
          },
          logout: {
            endpoint: '/api/auth/logout',
            method: 'DELETE',
            redirect: {
              success: '/',
              failure: null,
            }
          },
        })],
      forms: {
        login: {
          rememberMe: true,
        },
      },
    }),
    NbThemeModule.forRoot({
      name: 'uBid'
    }),
    NbMenuModule.forRoot(),
    NbLayoutModule,
    NbEvaIconsModule,
    AuthModule,
    SharedModule,
    CoreModule,
  ],
  providers: [
    { provide: OfferService, useClass: InfrastructureOfferService, deps: [ HttpClient ] },
    { provide: CategoryService, useClass: InfrastructureCategoryService, deps: [ HttpClient ] },
    { provide: BidService, useClass: InfrastructureBidService, deps: [ HttpClient ] },
    { provide: NotificationService, useClass: InfrastructureNotificationService, deps: [ NbToastrService ] },
    { provide: AuthService, useClass: InfrastructureAuthService, deps: [ NbAuthService ] },
    { provide: MenuService, useClass: InfrastructureMenuService, deps: [ NbMenuService ] },
    { provide: SearchService, useClass: InfrastructureSearchService, deps: [ HttpClient, NbSearchService ] },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
