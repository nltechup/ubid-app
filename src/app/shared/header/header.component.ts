import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';

import { AuthService, MenuService } from '../../core/services';
import { Token, AuthResult, MenuItem } from '../../core/models';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private destroy$: Subject<void> = new Subject();
  public isAuthenticated: boolean;
  public token$: Observable<Token>;
  public items = [
    { title: 'Profil', link: 'profile' },
    { title: 'Deconectare' },
  ];
  constructor(private authService: AuthService, private router: Router, private menuService: MenuService) { }

  ngOnInit() {
    this.subscribeToAuthenticationChanges();
    this.subscribetoCtxMenuChanges();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private subscribeToAuthenticationChanges() {
    this.authService.onAuthStatusChanged()
      .pipe(
        takeUntil(this.destroy$),
      )
      .subscribe((value: boolean) => {
        this.isAuthenticated = value;
        this.token$ = this.authService.getJwtToken();
      });
  }

  private subscribetoCtxMenuChanges() {
    this.menuService.onMenuItemSelected()
      .pipe(
        filter(({ tag }) => tag === 'user-ctx'),
        takeUntil(this.destroy$),
      )
      .subscribe((title: MenuItem) => {
        this.handleCtxMenu(title);
      });
  }

  private handleCtxMenu(item: MenuItem): void {
    if (item.item.title === 'Deconectare') {
      this.router.navigate(['auth', 'logout']);
    }
  }
}
