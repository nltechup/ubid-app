import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';

import { OfferLatestVM } from '../../core/models';

@Component({
  selector: 'app-about-to-expire',
  templateUrl: './about-to-expire.component.html',
  styleUrls: ['./about-to-expire.component.scss']
})
export class AboutToExpireComponent implements OnInit {
  @Input() expiringOffers$: Observable<OfferLatestVM[]>;

  ngOnInit() {
  }
}
