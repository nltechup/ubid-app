import { Component, OnInit, ViewChild, ElementRef, OnDestroy, Renderer2, Input } from '@angular/core';
import { Subscription, timer, Observable } from 'rxjs';
import { OfferLatestVM } from 'src/app/core/models';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit, OnDestroy {
  @ViewChild('carousel', { static: true }) carousel: ElementRef;

  @Input() latestOffers$: Observable<OfferLatestVM[]>;

  private timerSub$: Subscription;

  private currDeg = 0;

  constructor(private renderer: Renderer2) { }

  ngOnInit() {
    this.timerSub$ = timer(0, environment.DASHBOARD_ROTATION_TIMEOUT).subscribe(t => {
      this.rotate();
    });
  }

  ngOnDestroy() {
    if (this.timerSub$) {
      this.timerSub$.unsubscribe();
    }
  }

  public rotate(): void {
    this.currDeg -= 60;

    const elem = this.carousel.nativeElement;
    this.renderer.setStyle(elem, '-webkit-transform', `rotateY(${this.currDeg}deg)`);
    this.renderer.setStyle(elem, '-moz-transform', `rotateY(${this.currDeg}deg)`);
    this.renderer.setStyle(elem, '-o-transform', `rotateY(${this.currDeg}deg`);
    this.renderer.setStyle(elem, 'transform', `rotateY(${this.currDeg}deg`);
  }
}
