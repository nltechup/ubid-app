import { Component, OnInit, OnDestroy, SecurityContext } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { CategoryService } from '../../core/services';
import { CategoryVM } from '../../core/models/category-view.model';

@Component({
  selector: 'app-category-menu',
  templateUrl: './category-menu.component.html',
  styleUrls: ['./category-menu.component.scss']
})
export class CategoryMenuComponent implements OnInit, OnDestroy {
  private destroy$: Subject<void> = new Subject();
  public items: Array<{title: string, icon?: string, link?: string[]}> = [];

  constructor(private categoryService: CategoryService) { }

  ngOnInit() {
    this.categoryService.getCategoriesForMenu()
      .pipe(
        takeUntil(this.destroy$),
      )
      .subscribe((categories: CategoryVM[]) => {
        categories.forEach((category: CategoryVM) => {
          this.items.push({ title: category.name, link: [ `../offers/${category.link}` ] });
        });
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
