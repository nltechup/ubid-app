import { Component, OnInit, Input } from '@angular/core';

import { OfferLatestVM } from '../../core/models';

@Component({
  selector: 'app-offer-card',
  templateUrl: './offer-card.component.html',
  styleUrls: ['./offer-card.component.scss']
})
export class OfferCardComponent implements OnInit {
  @Input() offer: OfferLatestVM;
  @Input() view: string;
  constructor() { }

  ngOnInit() {
  }
}
