import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from '../core/guards/auth.guard';
import { ProfileDashboardComponent } from './profile-dashboard/profile-dashboard.component';

const routes: Routes = [
    { path: '', component: ProfileDashboardComponent, canActivate: [ AuthGuard ] },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProfileRoutingModule { }
