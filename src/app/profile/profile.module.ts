import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileDashboardComponent } from './profile-dashboard/profile-dashboard.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { SharedModule } from '../shared/shared.module';
import { CoreModule } from '../core/core.module';
import { ProfileOffersComponent } from './profile-offers/profile-offers.component';
import { ProfileBidsComponent } from './profile-bids/profile-bids.component';

@NgModule({
  declarations: [ProfileDashboardComponent, ProfileOffersComponent, ProfileBidsComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    SharedModule,
  ],
})
export class ProfileModule { }
