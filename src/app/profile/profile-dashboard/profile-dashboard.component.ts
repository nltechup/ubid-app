import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { OfferService, BidService } from '../../core/services';
import { BidModel, OfferWithBids } from '../../core/models';

@Component({
  selector: 'app-profile-dashboard',
  templateUrl: './profile-dashboard.component.html',
  styleUrls: ['./profile-dashboard.component.scss']
})
export class ProfileDashboardComponent implements OnInit {
  public myOffers$: Observable<OfferWithBids[]> = new Observable<OfferWithBids[]>();
  public myBids$: Observable<BidModel[]> = new Observable<BidModel[]>();

  constructor(private offerService: OfferService, private bidService: BidService) { }

  ngOnInit() {
    this.myOffers$ = this.offerService.getMyOffers();
    this.myBids$ = this.bidService.getMyBids();
  }
}
