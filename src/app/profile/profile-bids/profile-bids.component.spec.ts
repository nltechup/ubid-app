import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileBidsComponent } from './profile-bids.component';

describe('ProfileBidsComponent', () => {
  let component: ProfileBidsComponent;
  let fixture: ComponentFixture<ProfileBidsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileBidsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileBidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
