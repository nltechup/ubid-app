import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

import { BidModel } from '../../core/models';

@Component({
  selector: 'app-profile-bids',
  templateUrl: './profile-bids.component.html',
  styleUrls: ['./profile-bids.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileBidsComponent implements OnInit {
  @Input() myBids: BidModel[];

  constructor() { }
  ngOnInit() {
  }
}
