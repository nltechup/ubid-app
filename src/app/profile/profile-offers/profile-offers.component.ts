import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

import { OfferWithBids } from '../../core/models';

@Component({
  selector: 'app-profile-offers',
  templateUrl: './profile-offers.component.html',
  styleUrls: ['./profile-offers.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileOffersComponent implements OnInit {

  @Input() myOffers: OfferWithBids[];
  constructor() { }

  ngOnInit() {
  }
}
