FROM node:10.16.0-alpine as build
WORKDIR /app/
ADD ["angular.json", "tsconfig.json", "tsconfig.app.json", "tsconfig.spec.json", "tslint.json", "package.json", "package-lock.json", "./"]
RUN npm i
ADD src/ src/
RUN ./node_modules/@angular/cli/bin/ng build --prod

FROM nginx:1.16.0-alpine
COPY --from=build /app/dist/uBid-App /usr/share/nginx/html